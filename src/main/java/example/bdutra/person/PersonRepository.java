package example.bdutra.person;

import example.bdutra.model.PersonEntity;
import example.bdutra.repository.Repository;

public class PersonRepository extends Repository<PersonEntity> {

	public PersonRepository() {
		super(PersonEntity.class);
	}
}
