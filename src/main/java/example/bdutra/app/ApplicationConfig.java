package example.bdutra.app;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Alexis Binuelo
 *
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application{

}
