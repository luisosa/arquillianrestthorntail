package example.bdutra.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import example.bdutra.service.StaticService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Alexis Binuelo
 *
 */
@ApplicationScoped
@Path("/app")
@Api(value = "app")
public class PersonController {
	
	@Inject
	private StaticService staticService;
	
	@GET
    @Path("/v1")
    @Produces("text/plain")
    @ApiOperation(value = "Responde Hola Mundo usando GET", produces ="text/plain", response = Response.class, notes = "Descripcion larga")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Peticion Incorrecta"),
            @ApiResponse(code = 404, message = "No existe el recurso solicitado")
    })
	 public Response get() {
        return Response.ok(200).entity(staticService.getService()).build();
    }

	
}
