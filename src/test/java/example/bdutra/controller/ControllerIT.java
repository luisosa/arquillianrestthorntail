package example.bdutra.controller;

import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;
import java.net.URL;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * @author Alexis Binuelo
 *
 */
@RunWith(Arquillian.class)
public class ControllerIT {
    
    private Client client = ClientBuilder.newClient();

	
	private static final Logger LOG = LoggerFactory.getLogger(ControllerIT.class);
	
	 @ArquillianResource
	 private URL pathController; 
	 
	 @Test
	 @RunAsClient
	 public void getUrl() throws URISyntaxException {
		 LOG.info(" URL :{}",pathController.toURI());

		 ResteasyClient client = new ResteasyClientBuilder().build();
		 ResteasyWebTarget target = client.target(pathController.toURI()).path("/api/app/v1");
	    LOG.info(" URL :{}",target.getUri());
		 Response response = target.request().get();
		 LOG.info(" STATUS :{}",response.getStatus());

		 assertTrue(response.getStatus()==HttpStatus.SC_OK);
	 }
	 
	 
	 private static Client createClient() {
		    return ClientBuilder
		            .newBuilder()
		            .register(JacksonJaxbJsonProvider.class) //para procesar las peticiones como JSON
		            .build();
		}
}
