package example.bdutra.it.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import example.bdutra.model.PersonEntity;
import example.bdutra.person.PersonRepository;

@RunWith(Arquillian.class)
public class PersonRepositoryTest  {
	
	@Inject
	private PersonRepository personRepository;

	@Test
	@InSequence(1)
	public void testSavePerson() {
				
		PersonEntity person = new PersonEntity();
		
		person.setName("Bruno");
		person.setSurname("Dutra");
		
		personRepository.create(person);
	}
	
	@Test
	@InSequence(2)
	public void testListPersons() {
		
		List<PersonEntity> persons = personRepository.findAll();
		
		assertEquals(persons.size(), 1);
		
	}
	
}
