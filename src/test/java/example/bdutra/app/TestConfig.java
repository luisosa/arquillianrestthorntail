package example.bdutra.app;

import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;

@ArquillianSuiteDeployment
public class TestConfig {

	@Deployment
	public static WebArchive  createDeployment() {

		return ShrinkWrap.create(WebArchive.class,"demo.war")
				.addPackages(true, "example.bdutra")
				.addAsResource("META-INF/beans.xml")
				.addAsResource("META-INF/it-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("project-it.yml", "project-defaults.yml")
				.addAsResource("META-INF/arquillian.xml");
		}

}
